+++
date = "2017-03-01T16:24:52+07:00"
title = "Cara menanam tanaman hidropinik"
categories = ["Hidroponik"]
tags = ["tanaman hidroponik","hidroponik tanaman"]
author = "author"
description = "description"

+++

Cara Menanam Hidroponik di Rumah untuk Pemula | Selamat datang di blog lintangsore.com, tempat berbagi dan belajar tentang hobi berkebun, menanam buah-buahan dan sayuran. Pada kesempatan kali ini kita akan belajar menanam hidroponik sederhana yang bisa diaplikasikan secara mudah di rumah atau di pekarangan. Teknik cara menanam hidroponik menggunakan media air ini ditujukan untuk para pemula yang baru saja mengenal tanaman hidroponik dan ingin mencoba menanam sayuran hidroponik seperti cabe, kangkung, tomat dan lain-lain. Maka nantinya dalam teknik hidroponik ini kita tidak akan memerlukan peralatan ataupun bahan-bahan ideal yang sulit diperoleh tapi hanya memanfaatkan apa yang sudah ada di sekitar anda seperti botol bekas, botol gantung dan media tanam sederhana lainnya.

cara berkebun hidroponik di rumah
cara menanam hidroponik di rumah

Budidaya Tanaman Hidroponik
Sebelum kita membahas lebih lanjut tentang cara menanam hidroponik sederhana di rumah atau pekarangan, ada baiknya anda mengenal dulu apa itu bertanam hidroponik. Secara sederhana ditinjau dari asal katanya budidaya Hidroponik berarti suatu metode budidaya tanaman tanpa menggunakan media tanah, tetapi memanfaatkan air/larutan mineral bernutrisi yang diperlukan oleh tanaman dan bahan lainnya sebagai pengganti media tanah yang mengandung unsur hara seperti sabut kelapa, serat mineral, pasir, pecahan genteng/batu bata, serbuk kayu, dan lain sebagainya.

Di bawah ini beberapa kelebihan dan alasan untuk menguatkan motivasi anda belajar menanam tanaman dengan cara hidroponik, antara lain:

Bertanam hidroponik terbukti hemat dibandingkan dengan menanam konvensional di atas tanah karena tidak perlu menyiramkan air setiap hari sebab larutan nutrisi/media larutan mineral yang dipergunakan sudah tertampung di dalam wadah yang dipakai, sehingga kita tinggal melakukan pengontrolan saja.
Bertanam hidroponik dapat memaksimalkan lahan terbatas karena tidak membutuhkan lahan yang banyak, bahkan media tanaman bisa dibuat secara bertingkat
Bertanam hidroponik terbukti ramah lingkungan karena tidak menggunakan pestisida atau obat hama yang dapat merusak tanah, menggunakan air hanya 1/20 dari tanaman biasa, dan mengurangi CO2 karena tidak perlu menggunakan kendaraan atau mesin.
Tanaman hidroponik tidak merusak tanah karena tidak menggunakan media tanah dan juga tidak membutuhkan tempat yang luas.
Hasil tanaman hidroponik bisa dimakan secara keseluruhan termasuk akar karena terbebas dari kotoran dan hama
Bisa memeriksa akar tanaman dengan jelas secara periodik untuk mengontrol pertumbuhannya
Pertumbuhan tanaman lebih cepat dan kualitas hasil tanaman dapat terkontrol
Untuk menanam hidroponik tidak perlu tergantung musim, karena itu dapat ditanam kapan saja sesuai dengan planning kita.
Menanam hidroponik bisa mengurangi/menghemat pemakaian pupuk.
Bertanam hidroponik tidak perlu banyak tenaga untuk mengerjakannya
Lingkungan tempat bertanam hidroponik cenderung lebih bersih ketimbang bertanam di atas tanah.
Tanaman hidroponik jarang mempunyai masalah dengan hama dan penyakit tanaman yang disebabkan oleh bakteri, ulat dan cacing nematoda yang banyak terdapat dalam tanah
Lahan tempat menanam hidroponik lebih fleksibel, dapat ditanam di mana saja seperti di dalam rumah atau di pekarangan yang sudah dipaving.

cara menanam hidroponik sederhana di rumah
cara menanam hidroponik sederhana di rumah

cara menanam hidroponik sawi
cara menanam hidroponik sawi

Beberapa tanaman yang sering ditanam secara hidroponik, adalah sayur-sayuran hijau seperti selada, bayam, lombok, tomat, bak choy, brokoli, sawi, kailan, kangkung, bawang, strowbery, dan lain-lain. Tanaman-tanaman hidroponik di atas seringkali menjadi pilihan utama bagi para vegetarian yang sangat memperhatikan proses pembuatan makanan, apakah ada unsur kimiawi di dalamnya, apakah terdapat pembunuhan hewan, juga terkait dengan konservasi lingkungan dan usaha penghijauan.

Cara Menanam Sayuran Hidroponik

Ada dua macam teknik utama dalam cara bercocok tanam hidroponik.

Teknik menanam hidroponik menggunakan larutan nutrisi
Teknik menanam hidroponik menggunakan media pengganti


Perbedaan mendasar dari kedua teknik di atas adalah sebagai berikut:

Pada teknik yang pertama, kita tidak membutuhkan media tanam keras sebagai tempat pertumbuhan akarnya, tetapi cukup menggunakan media larutan nutrisi/air. Metode yang menggunakan larutan tidak membutuhkan media keras untuk pertumbuhan akar, hanya cukup dengan larutan mineral bernutrisi. Contoh cara dalam teknik larutan yang umum dipakai adalah teknik larutan statis dan teknik larutan alir.

Sedangkan untuk teknik yang kedua, kita menggunakan media substitusi untuk menggantikan peran tanah sebagai tempat pertumbuhan akar tanaman. Dalam hal ini kita bisa memanfaatkan media sabut kelapa, akr/batang pakis, pasir, pecahan batu bata/genteng , serbuk kayu, dan lain-lain sebagainya.

Pada postingan yang ini kita tidak akan membahas keduanya karena akan menjadikan postingan ini terlalu panjang dan pastinya membuat anda lelah membacanya. maka, kita hanya akan membahas teknik yang pertama saja yaitu cara menanam hidroponik menggunakan media air/larutan.